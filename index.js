const express = require('express')
const app = express()
const request = require('request')
var nano = require('nano')('http://localhost:5984');
var trackDB = nano.db.use('test');
var crypto = require('crypto');


 var bodyParser = require('body-parser')
 app.use(bodyParser.json());

app.post('/auth', (req, res) => {
	console.log(req.body)
	let userType = req.body.userType;
	let username = req.body.username;
	let password = getHash(req.body.password);
	trackDB.view('auth', userType,{
     'key':username}, function(err, body) {
     	console.log("response from trackDB",err, body);
      if (!err) {
	    let userRecord = body.rows[0];
	    if(userRecord && (userRecord.value == password)){
	    	res.status(200).send('OK');
	    }
	    else{
	    	res.status(401).send(`username or password is not valid for: ${username}`);
	    }
	  } 
	  else{
		console.log(`username or password is not valid for: ${username}`);
		res.status(401).send(`user: ${username} does not exist`);
	  }
	});
})

app.get('/getClients', (req, res) =>{
	console.log(req.query);
	let agent = req.query.agent;
	let agentClients = [];
	trackDB.view('client', 'clients', {
 	 'include_docs': true
	}, function(err, body) {
	  console.log("response from trackDB",err, body);
      if (!err) {
      	console.log(body.rows);
	    body.rows.forEach(function(doc) {
			if(doc.doc && doc.doc.agent == agent){
				agentClients.push(doc.doc);
			}	    	
    	});
	    console.log(agentClients, agent)
		res.status(200).send(JSON.stringify(agentClients))
      } else{
		console.log('Err', err)
		res.status(402).send('Error in getting clients');	
	  } 
	});

})

app.post('/createOrUpdateClient', (req, res) =>{
	let data = req.body;
	if(data.update){
		delete data["update"];
		trackDB.get(data["_id"], function(err, body) {
			console.log("response from trackDB",err, body);
  			data["_rev"] = body["_rev"];
			trackDB.insert(data, function(err, body) {
				console.log("response from trackDB",err, body);
			    if (!err) {
			    		res.status(200).send('OK')
				} else {
					res.status(500).send('Error in updating doc');
				}
			});  			
		});
	}
	else {
		trackDB.insert(data, function(err, body) {
			console.log("response from trackDB",err, body);
   		    if (!err) {
    			res.status(200).send('OK')
			}
		     else {
		  		res.status(500).send('Error in creating doc');
		  }
		});	
	}
})

function getHash(data) {
	let generatedHash = crypto.createHash("sha256").update(data).digest("base64");
	console.log(generatedHash)
    return generatedHash;
}

app.listen(3000, () => console.log('Example app listening on port 3000!'))

